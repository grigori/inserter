import logging
import json
from flask import Flask, request
import pika
import gridfs
from pymongo import MongoClient
from flask import make_response
from flask import render_template

app = Flask(__name__)

def json_response(data='', status=200, headers=None):
    headers = headers or {}
    if 'Content-Type' not in headers:
        headers['Content-Type'] = 'multipart/form-data'

    return make_response(data, status, headers)

@app.route('/upload')
def upload_file():
    return render_template('uploader.html')


@app.route('/uploader', methods=['GET', 'POST'])
def upload_file_my():
    if request.method == 'POST':
        f = request.files['file']
        app.logger.info("got POST request.")
        client = MongoClient("mongodb://mongodb:27017")
        app.logger.info("mongo client connected")
        db = client['files_db']
        app.logger.info("mongo db connected")

        fs = gridfs.GridFS(db)
        app.logger.info("mongo gridfs ")
        frame_id = fs.put(f.read(), filename=f.filename)
        return 'file uploaded successfully'


@app.route("/", methods=['POST'])
def insert_frame():
    app.logger.debug('this is a DEBUG message')
    if 'multipart/form-data' not in request.content_type:
        error = json.dumps({'error': 'Invalid Content Type: {}'.format(request.content_type)})
        return json_response(error, 400)

    try:
        got_file = request.files['file']
        filename = got_file.filename
        file = got_file.read()
        print("Inserting %s" % filename)
        print("Authorization: " + request.headers.environ['HTTP_AUTHORIZATION'])

        credentials = pika.PlainCredentials('guest', 'guest')
        connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq', 5672, '/', credentials))
        channel = connection.channel()

        channel.queue_declare(queue='myQ')

        client = MongoClient("mongodb://mongodb:27017")
        db = client['files_db']
        results = db['results']

        fs = gridfs.GridFS(db)
        _id = fs.put(file, filename=filename)
        _obj = {
            '_id':_id,
            'file': (filename, file, 'multipart/form-data'),
        }

        with client.start_session() as session:
            session.start_transaction()
            results.insert_one(_obj)
            session.commit_transaction()

        channel.basic_publish(exchange='amq.direct',
                              routing_key='myQ',
                              body=str(_id))
        connection.close()
    except Exception as e:
        app.logger.error(e)
        error = json.dumps({'error': 'Exception: {}'.format(str(e))})
        return json_response(error, 400)


if __name__ != "__main__":
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

if __name__ == "__main__":
    app.run(host='0.0.0.0')

